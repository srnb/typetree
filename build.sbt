import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

scalaVersion in ThisBuild := "2.11.12"

sourceDirectories in Compile := Nil
sourceDirectories in Test := Nil

val shared = Seq(
  scalaVersion := "2.11.12",
  organization := "tf.bug",
  name         := "typetree",
  version      := "0.1.0",
  libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value
)

lazy val typetree = crossProject(JSPlatform, JVMPlatform, NativePlatform)
  .crossType(CrossType.Pure)
  .in(file("."))
  .settings(shared)
  .jsSettings(crossScalaVersions := Seq("2.11.12", "2.12.7"))
  .jvmSettings(crossScalaVersions := Seq("2.11.12", "2.12.7"))
  .nativeSettings(crossScalaVersions := Seq("2.11.12"))

lazy val typetreeJS = typetree.js
lazy val typetreeJVM = typetree.jvm
lazy val typetreeNative = typetree.native
